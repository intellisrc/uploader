package com.intellisrc.upload

import com.intellisrc.core.Log
import com.intellisrc.core.SysClock
import com.intellisrc.etc.Cache
import com.intellisrc.etc.Mime
import com.intellisrc.etc.YAML
import com.intellisrc.web.Service
import com.intellisrc.web.ServiceOutput
import com.intellisrc.web.ServiciableMultiple
import com.intellisrc.web.UploadFile
import groovy.transform.CompileStatic
import spark.Request
import spark.Response

import java.time.LocalDateTime

import static com.intellisrc.upload.Settings.*

/**
 * @since 2021/12/02.
 */
@CompileStatic
class UploadService implements ServiciableMultiple {
    static void cleanFiles() {
        uploadDir.eachFileMatchAsync("*.meta") {
            File metaFile ->
                Map meta= YAML.decode(metaFile.text) as Map
                switch (meta.rule) {
                    case "time": // specific time
                        LocalDateTime expirationTime = meta.time.toString().toDateTime()
                        if (expirationTime > SysClock.now) {
                            downloadDone(metaFile)
                        }
                        break
                    case "times": // downloaded times
                        int times = meta.times as int
                        if(times <= 0) {
                            downloadDone(metaFile)
                        }
                        break
                }
        }
    }
    static void downloadDone(File metaFile) {
        File doneFile = File.get(uploadDir, metaFile.name.replace(".meta", ".done"))
        File file = File.get(uploadDir, metaFile.name.replace(".meta", ""))
        if(doneFile.exists()) {
            doneFile.delete()
            Log.w("Previous 'done' file was removed: %s", doneFile.name)
        }
        metaFile.renameTo(doneFile)
        if(file.exists()) {
            file.delete()
        }
    }

    @Override
    List<Service> getServices() {
        return [
            new Service(
                path: "/favicon.ico",
                cacheTime: Cache.FOREVER,
                action: {
                    return File.get("resources", "public", "img", "favicon.png")
                }
            ),
            new Service(
                path : "/upload",
                allow: AuthService.admin,
                action: {
                    return File.get("resources", "private", "html", "admin.html")
                }
            ),
            new Service (
                path: "/upload",
                method : Service.Method.POST,
                allow: AuthService.admin,
                action: {
                    List<UploadFile> files, Request request ->
                        String rule = request.queryParams("rule") ?: "none"
                        String baseURL = request.url().substring(0, request.url().length() - request.uri().length())
                        Map response = [ ok : false ]
                        Map<String, String> uploaded = [:]
                        if(! files.empty) {
                            try {
                                Log.i("Uploading %d files...", files.size())
                                response.ok = true
                                files.each {
                                    UploadFile file ->
                                        if (response.ok && file.exists() &&! file.empty) {
                                            Map meta = [:]
                                            Log.i("Uploading : %s, Size: %d by %s", file.originalName, file.size(), request.ip())
                                            String hash = file.bytes.sha256()
                                            File tgtFile = File.get(uploadDir, hash)
                                            meta.name = file.originalName
                                            meta.date = SysClock.now.YMDHms
                                            meta.from = request.ip()
                                            meta.rule = rule
                                            switch (rule){
                                                case "time":
                                                    meta[rule] = SysClock.now.plusSeconds((request.queryParams(rule) ?: "0") as int)
                                                    break
                                                default:
                                                    meta[rule] = request.queryParams(rule) ?: ""
                                                    break
                                            }
                                            if (tgtFile.exists()) {
                                                Log.i("Exact file existed. Ignored")
                                                file.delete()
                                            } else {
                                                response.ok = file.moveTo(tgtFile)
                                            }
                                            if (response.ok) {
                                                URL url = Downloader.getLink(baseURL, tgtFile)
                                                uploaded[meta.name.toString()] = url.toExternalForm()
                                                Log.i("Uploaded: %s, URL: %s", meta.name, url)
                                                File.get(uploadDir, hash + ".meta").text = YAML.encode(meta)
                                            } else {
                                                response.ok = false
                                                Log.w("Unable to write file: %s", tgtFile.absolutePath)
                                            }
                                        }
                                }
                                if (! uploaded.isEmpty()) {
                                    MsgSender.sendUpload(uploaded)
                                    if (downloadEnabled) {
                                        response.links = uploaded
                                    }
                                }
                            } catch (Exception e) {
                                Log.e("Unable to upload", e)
                            }
                        } else {
                            Log.w("Server didn't received any file.")
                        }
                        return response
                }
            ),
            new Service (
                path: "/download/:nameHash/:fileHash",
                download: true,
                action: {
                    Request request, Response response ->
                        cleanFiles()
                        String nameHash = request.params("nameHash")
                        String fileHash = request.params("fileHash")
                        Optional<File> file = Downloader.getFile(nameHash, fileHash)
                        if (!file.empty && file.get().exists()) {
                            File metaFile = File.get(uploadDir, file.get().name + ".meta")
                            Map meta
                            if(metaFile.exists()) {
                                meta = YAML.decode(metaFile.text) as Map
                            } else {
                                meta = [
                                    rule : "none"
                                ]
                                Log.w("Meta file of: %s didn't exists", file.get().name)
                            }
                            boolean download = true
                            switch (meta.rule) {
                                case "time": // specific time
                                    LocalDateTime expirationTime = meta.time.toString().toDateTime()
                                    download = (expirationTime <= SysClock.now)
                                    break
                                case "times": // downloaded times
                                    int times = meta.times as int
                                    download = times > 0
                                    meta.times = times - 1
                                    break
                            }
                            if(downloadEnabled && download) {
                                String baseURL = request.url().substring(0, request.url().length() - request.uri().length())
                                Log.i("Downloaded: %s by %s", meta.name, request.ip())
                                meta.down = ((meta.down ?: []) as List) + [ time : SysClock.now.YMDHms, from : request.ip() ]
                                MsgSender.sendDownload(request.ip(), meta.name.toString(), Downloader.getLink(baseURL, file.get(), true).toExternalForm())
                                metaFile.text = YAML.encode(meta)
                                return new ServiceOutput(
                                    content     : file.get().bytes,
                                    fileName    : meta.name,
                                    size        : file.get().size(),
                                    etag        : file.get().name.substring(0, 8)
                                )
                            } else {
                                response.status(404)
                                Log.w("File expired: %s. Requested by: %s (%s)", meta.name, request.ip(), request.userAgent())
                                downloadDone(metaFile)
                                return null
                            }
                        } else {
                            response.status(404)
                            Log.w("File not found or invalid: %s. Requested by: %s (%s)", nameHash, request.ip(), request.userAgent())
                            return null
                        }
                }
            ),
            new Service(
                path : "/delete/:fileHash/:hash",
                allow: AuthService.admin,
                etag: null,
                action: {
                    Request request ->
                        Optional<File> file = Downloader.getFile(request.params("fileHash"), request.params("hash"))
                        if(! file.empty) {
                            Log.i("File: %s deleted by %s", file.get().name, request.ip())
                        }
                        boolean ok = ! file.empty && file.get().delete()
                        return "Deleted: " + (ok ? "YES" : "NO")
                }
            ),
            new Service(
                path : "/exit",
                allow: AuthService.admin,
                action: {
                    Log.w("Exit requested by user")
                    System.exit(1)
                }
            )
        ]
    }
}
