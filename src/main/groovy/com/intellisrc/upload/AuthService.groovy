package com.intellisrc.upload

import com.intellisrc.core.Log
import com.intellisrc.crypt.hash.PasswordHash
import com.intellisrc.web.Service
import com.intellisrc.web.ServiciableAuth
import groovy.transform.CompileStatic
import spark.Request
import spark.Response
import static com.intellisrc.upload.Settings.*

/**
 * @since 2020/12/18.
 */
@CompileStatic
class AuthService implements ServiciableAuth {
    String rootPath = ""
    String loginPath = "/login"
    String logoutPath = "/logout"

    static enum Level {
        GUEST, ADMIN
    }
    static final Service.Allow admin = {
        Request request ->
            if(request.session()) {
                return getLevel(request) >= Level.ADMIN
            } else {
                return false
            }
    } as Service.Allow

    @Override
    Map<String,Object> onLogin(final Request request, final Response response) {
        String pass = request.queryParams("p") ?: ""

        Level level = Level.GUEST
        if(pass.empty) {
            Log.w("Password is empty.")
        } else {
            level = onLoginAuth(pass)
        }
        if(level == Level.GUEST) {
            Log.w("[%s] Password failed", request.ip())
        }
        return (level > Level.GUEST ? [ level : "admin" ]  : [:]) as Map<String, Object>
    }

    @Override
    boolean onLogout(Request request, Response response) { return true }

    static Level onLoginAuth(String password) {
        Level level = Level.GUEST
        if(isPasswordCorrect(password)) {
            level = Level.ADMIN
        }
        return level
    }

    static boolean isPasswordCorrect(String pass) {
        char[] pwd = pass.toCharArray()
        PasswordHash ph = new PasswordHash(password: pwd)
        return loginHash.empty ? false : ph.verify(loginHash)
    }

    static String getPasswordHash(char[] pwd) {
        PasswordHash ph = new PasswordHash(password: pwd)
        return ph.BCryptNoHeader()
    }

    /**
     * Return User Level according to request
     * @param request
     * @return
     */
    static Level getLevel(Request request) {
        Level level = Level.GUEST
        String strLevel = request?.session()?.attribute("level")?.toString() ?: ""
        if(strLevel) {
            level = strLevel.toUpperCase() as Level
        }
        return level
    }

    @Override
    String getPath() { rootPath }

    @Override
    String getLoginPath() { loginPath }

    @Override
    String getLogoutPath() { logoutPath }
}
