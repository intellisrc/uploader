package com.intellisrc.upload

import com.intellisrc.core.Config
import com.intellisrc.core.Log
import com.intellisrc.net.Email
import com.intellisrc.net.Smtp
import groovy.transform.CompileStatic
import static com.intellisrc.upload.Settings.*

/**
 * @since 2021/12/03.
 */
@CompileStatic
class MsgSender {
    /**
     * Send Upload notification
     * @return
     */
    static boolean sendUpload(Map<String, String> files) {
        boolean sent = true
        if(sendOnUpload) {
            Smtp smtp = new Smtp()
            List<String> text = []
            List<String> html = []
            files.each {
                text << "Uploaded file: " + it.key
                text << "\tDownload link: " + it.value
                html << "Uploaded file: <a href='${it.value}'>${it.key}</a>".toString()
            }
            List<Email> emails = []
            if (receiverMail) {
                emails << new Email(receiverMail)
            } else {
                emails << new Email(Config.get("mail.smtp.to", Config.get("mail.smtp.from")))
            }
            if (bccMail) {
                emails << new Email(bccMail)
            }
            sent = smtp.send(emails, subjectUpload, html.join("<br>"), text.join("\n"))
            if (!sent) {
                Log.w("Unable to send email")
            }
        }
        return sent
    }
    /**
     * Send Download notification
     * @return
     */
    static boolean sendDownload(String ip, String file, String link) {
        boolean sent = true
        if(sendOnDownload) {
            Smtp smtp = new Smtp()
            List<String> text = []
            List<String> html = []
            text << "Downloaded file: " + file
            text << "\tBy : " + ip
            text << "\tDelete link: " + link
            html << "File Downloaded by ${ip}. Delete : <a href='${link}'>${file}</a>".toString()
            List<Email> emails = []
            if (receiverMail) {
                emails << new Email(receiverMail)
            } else {
                emails << new Email(Config.get("mail.smtp.to", Config.get("mail.smtp.from")))
            }
            if (bccMail) {
                emails << new Email(bccMail)
            }
            sent = smtp.send(emails, subjectDownload, html.join("<br>"), text.join("\n"))
            if (!sent) {
                Log.w("Unable to send email")
            }
        } else {
            Log.v("Send in download event is disabled")
        }
        return sent
    }
}
