package com.intellisrc.upload

import com.intellisrc.core.Log
import com.intellisrc.etc.Bytes
import groovy.transform.CompileStatic
import org.apache.commons.codec.digest.DigestUtils
import java.nio.file.Files
import static com.intellisrc.upload.Settings.*

/**
 * @since 2021/12/03.
 */
@CompileStatic
class Downloader {

    static URL getLink(String urlBase, File file, boolean delete = false) {
        if(https) { urlBase = urlBase.replace("http:", "https:") }
        String path = delete ? "/delete/" : "/download/"
        return (urlBase + path + getHash(file.name) + "/" + getHash(file)).toURL()
    }

    static Optional<File> getFile(String fileNameHash, String hash) {
        File file = uploadDir.listFiles().find {getHash(it.name) == fileNameHash }
        if(file) {
            if(getHash(file) != hash) {
                file = null
            }
        }
        return Optional.ofNullable(file)
    }

    protected static String getHash(String name) {
        return Bytes.concat(downPepper.bytes, name.bytes).md5()
    }

    protected static String getHash(File file) {
        String md5 = ""
        try {
            InputStream is = Files.newInputStream(file.toPath())
            md5 = DigestUtils.md5Hex(is)
            is.close()
        } catch(Exception e) {
            Log.w("Unable to calculate hash: %s (%s)", file.name, e.message)
        }
        return md5
    }
}
