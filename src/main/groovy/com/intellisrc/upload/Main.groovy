package com.intellisrc.upload

import com.intellisrc.core.SysService
import com.intellisrc.web.WebService
import groovy.transform.CompileStatic
import static com.intellisrc.upload.Settings.*

/**
 * @since 2021/12/02.
 */
@CompileStatic
class Main extends SysService {
    static {
        service = new Main()
    }
    static WebService ws
    @Override
    void onStart() {
        init()
        ws = new WebService(
            port: webPort,
            resources : File.get("resources", "public")
        ).add(new AuthService())
         .add(new UploadService())
         .start()
    }

    @Override
    void onStop() {
        ws.stop()
    }
}
