package com.intellisrc.upload

import com.intellisrc.core.Config
import com.intellisrc.core.Log
import com.intellisrc.crypt.Crypt
import com.intellisrc.etc.Bytes
import groovy.transform.CompileStatic
/**
 * @since 2022/05/25.
 */
@CompileStatic
class Settings {
    static final int webPort        = Config.get("port", 8000)
    static String password          = Config.any.get("password")
    static String loginHash         = Config.any.get("hash")
    static String downPepper        = Config.any.get("pepper")

    static boolean https            = Config.any.get("download.https", true)
    static boolean downloadEnabled  = Config.any.get("download.enabled", true)

    static String receiverMail      = Config.any.get("mail.to")
    static String bccMail           = Config.any.get("mail.bcc")
    static boolean sendOnUpload     = Config.any.get("mail.upload", false)
    static boolean sendOnDownload   = Config.any.get("mail.download", false)
    static String subjectUpload     = Config.any.get("mail.subject.upload", "↑ File Uploaded")
    static String subjectDownload   = Config.any.get("mail.subject.download", "↓ File Downloaded")

    static File uploadDir           = File.get("uploads")

    static void init() {
        if(! downPepper) {
            downPepper = Bytes.toString(Crypt.randomChars(64))
            Log.w("Pepper not used. Please specify PEPPER variable. Using this value: %s", downPepper)
        }
        if(! loginHash) {
            if(! password) {
                password = Bytes.toString(Crypt.randomChars(10))
                Log.w("Password was not set. Please specify PASSWORD variable. using: %s", password)
            }
            loginHash = AuthService.getPasswordHash(password.toCharArray())
            Log.i("You can replace PASSWORD for HASH with this value: %s", loginHash)
            password = ""
        }
        if(!uploadDir.exists()) {
            Log.i("Creating directory: %s", uploadDir.absolutePath)
            try {
                uploadDir.mkdirs()
            } catch(Exception e) {
                Log.w("Unable to create directory. %s", e.message)
            }
        }
    }
}
