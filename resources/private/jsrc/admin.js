m2d2.ready($ => {
    const links = false; //TODO: make it configurable
    const out = $("#out", {
        html : ""
    });
    const status = $("#status", {
        show : false,
        progress : {
            value : 0
        }
    });
    const rule = $("#rule", {
        items : (() => {
            const opts = [{ text : _("keep_forever"), value : "none:none" }];
            [1,2,3,5].forEach(i => {
                opts.push({
                    text : _("delete_after") + " " + i + " " + _("download" + (i == 1 ? "" : "s")),
                    value : "times:" + i
                });
            });
            [10,30].forEach(i => {
                opts.push({
                    text : _("delete_after") + " " + i + " " + _("minutes"),
                    value : "time:" + (i * 60)
                });
            });
            [1,4,8,12].forEach(i => {
                opts.push({
                    text : _("delete_after") + " " + i + " " + _("hour" + (i == 1 ? "" : "s")),
                    value : "time:" + (i * 3600)
                });
            });
            [1,2,3,7,15,30].forEach(i => {
                opts.push({
                    text : _("delete_after") + " " + i + " " + _("day" + (i == 1 ? "" : "s")),
                    value : "time:" + (i * 3600 * 24)
                });
            });
            return opts
        })()
    });
    const upload = $("#upload", {
        onclick : function(ev) {
            this.disabled = true;
            const ups = [];
            let totalFiles = 0;
            let uploaded = 0;
            const params = {};
            params[rule.value.split(':')[0]] = rule.value.split(':')[1];
            $.upload(ev, {
                field   : "file",
                upload  : "/upload",
                args    : params,
                parallel : true,
                maxParallel : 10,
                onSelect : (files) => {
                    uploaded = 0;
                    upload.disabled = true;
                    status.show = true;
                    totalFiles = 0;
                    let index = 0;
                    Array.from(files).forEach(file => {
                        ups[index] = "<div class='read'>" + ("READ" + " : " + file.name) + "</div>";
                        out.html = ups.join("<br>");
                        totalFiles++;
                    });
                },
                onUpdate : (pct, file, index) => {
                    ups[index] = "<div class='uploading'>" + (("" + pct).padEnd(3, " ") + "% : " + file.name) + "</div>";
                    out.html = ups.join("");
                    const progress = Math.round((uploaded / totalFiles) * 100);
                    status.progress.value = progress;
                },
                onResponse : (response) => {
                    return response.ok ? response.links : []
                },
                onDone : (response, allDone) => {
                    response.forEach(res => {
                        const hasData = res.data && res.data.length;
                        ups[res.index] = "<div class='done'>DONE" + " : " + (hasData ? "<a href='" + res.data + "' target='_blank'>" + res.file.name + "</a>" : res.file.name) + "</div>";
                        out.html = ups.join("");
                        uploaded++;
                    });
                    if(allDone) {
                        upload.disabled = false;
                        status.progress.value = 100;
                        setTimeout(() => {
                            status.show = false;
                        }, 5000);
                    }
                },
                onError : (response) => {
                    console.log(response);
                    //ups[index] = "FAIL" + " : " + file.name;
                    //out.html = ups.join("<br>");
                    upload.disabled = false;
                }
            });
            setTimeout(() => {
                upload.disabled = false;
            }, 1000);
            return false;
        }
    })
    $("#logout", {
        onclick : function() {
            $.get("/logout", () => {
                window.location.href = "/";
            });
        }
    })
});
