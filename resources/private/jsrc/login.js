m2d2.ready($ => {
    const flogin = $("#flogin", {
        pass : "",
        button : {
            disabled : false
        },
        onsubmit : function() {
            flogin.button.disabled = true;
            const data = this.getData();
            $.post("/login", { p : data.pass }, res => {
                if(res.ok) {
                    window.location.href = "/upload";
                } else {
                    $.failure(_("login_failed"), () => {
                        flogin.reset();
                    })
                }
            }, fail => {
                $.failure(_("login_failed"), () => {
                    flogin.reset();
                })
            }, false);
            return false;
        },
        reset : function() {
            this.button.disabled = false;
            this.pass.value = "";
            this.pass.focus();
        },
        onload : function() {
            this.pass.focus();
        }
    });
});