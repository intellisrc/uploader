const dictionary = {
    login: {
        en : "Login",
        ja : "ログイン"
    },
    logout: {
        en : "Logout",
        ja : "ログアウト"
    },
    password: {
        en : "Password",
        ja : "パスワード"
    },
    upload: {
        en : "Upload",
        ja : "アップロード"
    },
    upload_service: {
        en : "Upload Service",
        ja : "アップロードサービズ"
    },
    login_failed: {
        en : "Login Failed",
        ja : "ログインがしっぱいされました"
    },
    keep_forever: {
        en : "Keep Forever",
        ja : "永遠に保つ"
    },
    delete_after: {
        en : "Delete after",
        ja : "削除する時間"
    },
    download: {
        en : "download",
        ja : "ダウンロード"
    },
    downloads: {
        en : "downloads",
        ja : "ダウンロード"
    },
    minutes: {
        en : "minutes",
        ja : "分"
    },
    hour: {
        en : "hour",
        ja : "時"
    },
    hours: {
        en : "hours",
        ja : "時"
    },
    day: {
        en : "day",
        ja : "日"
    },
    days: {
        en : "days",
        ja : "日"
    }
}
const _ = m2d2.load().dict.set(dictionary);
m2d2.ready($ => {
    $.lang('en');
});