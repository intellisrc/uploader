# Upload Service

With this application you can upload files and share their links (with a hash) to be downloaded.
Optionally you can be notified when a file is uploaded or downloaded.

## Docker variables:

### Required
* PORT (default: 8000) : Web Service Port
* PASSWORD : Password used for upload files, if not set, it will be generated (see logs).
* PEPPER : if not set, the system will create a key which is used to generate hashes (see logs).

### Download related (optional)
* DOWNLOAD_ENABLED (default: true) : whether the system will display download links or not (only upload service).
* DOWNLOAD_HTTPS (default: true) : use https download links (this won't install a certificate automatically, it is used to rewrite urls).

### Mail related (optional)
* MAIL_UPLOAD (default: false) : send an email when files are uploaded
* MAIL_SUBJECT_UPLOAD : customized upload subject for emails
* MAIL_DOWNLOAD (default: false) : send an email when files are downloaded
* MAIL_SUBJECT_DOWNLOAD : customized download subject for emails
* MAIL_TO  : Email address to send alerts
* MAIL_BCC : Email address to send copy of alerts
* MAIL_SMTP_USER : SMTP user
* MAIL_SMTP_PASS : SMTP password
* MAIL_SMTP_HOST : SMTP hostname
* MAIL_SMTP_PORT : SMTP port
* MAIL_SMTP_FROM : Email address to use as "From"
* MAIL_SMTP_NAME : Sender Name to use in "From"




