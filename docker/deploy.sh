#!/bin/bash
# This script will create image and execute docker-compose (create network, etc..)
if [[ $1 == "prod" ]]; then
  # To add ssh support:
  # 1. add user to docker group
  # 2. install docker-compose in host
  # 3. link docker-compose (in /usr/local/bin to /usr/bin)
  # 4. restart docker
  # export DOCKER_HOST="ssh://lepe@docker.lh:8721"
  export DOCKER_HOST="ssh://docker@wasabi.alepe.com:8721"
  #export DOCKER_HOST="ssh://securemaster@gate.hanko21.info"
elif [[ $1 != "local" ]]; then
  echo "Usage: $0 [local|prod]"
  exit;
fi
docker-compose up -d --build
unset DOCKER_HOST

# docker-compose doesn't seems to have "context"
#CONTEXT=""
#if [[ $1 == "prod" ]]; then
  # Add context with: docker context create gate --docker "host=ssh://securemaster@gate.hanko21.info"
#  CONTEXT="--context gate"
#elif [[ $1 != "local" ]]; then
#  echo "Usage: $0 [local|prod]"
#  exit;
#fi
#docker-compose $CONTEXT up -d --buil
