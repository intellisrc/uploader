const gulp = require('gulp'),
    concat = require('gulp-concat'),
    terser = require('gulp-terser'),
    cssmin = require('gulp-cssmin'),
    rename = require('gulp-rename'),
    sass   = require('gulp-sass')(require('node-sass'));
    sassGlob = require('gulp-sass-glob'),
    maps   = require('gulp-sourcemaps');

const paths = {
    // Private in:
    priv    : "resources/private/",
    jsrc    : "jsrc/*.js",
    scss    : 'scss/*.scss',
    // Public out:
    pub     : "resources/public/",
    js      : 'js/',
    css     : 'css/',
};

// JS
gulp.task('js', gulp.series([], function() {
    return gulp.src([paths.priv + paths.jsrc])
        .pipe(terser())
        .pipe(rename({ extname : "" })) //remove extensions
        .pipe(rename({ extname : ".min.js" }))
        .pipe(gulp.dest(paths.pub + paths.js));
}));
// CSS
gulp.task('css', gulp.series([], function () {
    return gulp.src([paths.priv + paths.scss])
        .pipe(maps.init())
        .pipe(sassGlob())
        .pipe(sass().on('error', sass.logError))
        .pipe(cssmin())
        .pipe(rename({ extname : "" })) //remove extensions
        .pipe(rename({ extname : ".min.css" }))
        .pipe(maps.write("."))
        .pipe(gulp.dest(paths.pub + paths.css));
}));

// Build
gulp.task('default', gulp.series(['js','css']));
