#!/bin/bash

target=$1
if [[ $target == "" ]]; then
    target="local"
    echo "Exporting to Local"
fi
# This script prepares files for the docker container inside docker/ directory (based on deploy/)
#./compile
rsync -iaL --exclude=log --delete deploy/ docker/image/

if [[ $target != "local" ]]; then
  read -rp "Upload ? [y|N] " ans
else
  ans="Y"
fi
if [[ $ans == "Y" || $ans == "y" ]]; then
	echo "Uploading..."
	cd docker/ || exit
	./deploy.sh $target
	if [[ $? == 0 ]]; then
	  echo "Ready!"
	fi
fi
